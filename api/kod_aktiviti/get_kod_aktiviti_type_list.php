<?php

header("Access-Control-Allow-Origin: *");
include_once '../../vendor/autoload.php';
include_once '../config/db_connection.php';

$DBQueryObj = new DBQuery($host, $username, $password, $database_name);

$MagicInputObj = new MagicInput();

$MagicInputObj->setInputsDefinition([
    ['kod_program', 's', false, ''],
    ['parent', 's', false, ''],
    ['isParent', 's', false, '']
]);

$MagicInputObj->copy_GET_properties();

$condition = '';

if ($MagicInputObj->isInputsComplied()) {

    if (!is_null($MagicInputObj->kod_program)) {
        $id_program = mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->kod_program);

        if ($condition != '') {
            $condition .= " AND kod_program='{$id_program}'";
        } else {
            $condition = "WHERE kod_program='{$id_program}'";
        }
    }

    /**TODO: use isParent==1 if want the response to include parent with childs list */
    if (!is_null($MagicInputObj->parent)) {
        $parent = mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->parent);
        
        $isParent=0;
        
        if(!is_null($MagicInputObj->isParent)){
            $isParent = mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->isParent);
        }        

        if ($isParent == 1) {
            if ($condition != '') {
                $condition .= " AND parent='{$parent}' OR kod_aktiviti='{$parent}'";
            } else {
                $condition = "WHERE parent='{$parent}' OR kod_aktiviti='{$parent}'";
            }
        } else {
            if ($condition != '') {
                $condition .= " AND parent='{$parent}'";
            } else {
                $condition = "WHERE parent='{$parent}'";
            }
        }
    }


$sql = <<<SQL
SELECT
    `kod_aktiviti`,
    CONCAT(`kod_aktiviti`, ' - ', `perihal_aktiviti`) AS perihal_aktiviti
FROM
`tbl_aktiviti_ref`
$condition
ORDER BY kod_aktiviti
SQL;

//echo $sql;
//exit();

    $DBQueryObj->setSQL_Statement($sql);

    $DBQueryObj->runSQL_Query();

    if ($DBQueryObj->isHavingRecordRow()) {
        echo $DBQueryObj->getRowsInJSON();
    } else {
        echo '[]';
    }
}