<?php
class KodAktivitiPaging extends UniversalPaging{
    
    protected function handleTaskOnNoPaging() {
        echo '[]';
    }

    protected function performTaskOnEachPage(\DBQuery $DBQueryObj, $startRowIndex, $lastRowIndex) {
        echo $DBQueryObj->getRowsInJSON();
    }

}

