<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');

include_once '../../vendor/autoload.php';
include_once '../config/db_connection.php';

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    echo 'preflight';exit;
}

$MagicInputObj = new MagicInput();
$MagicInputObj->copy_RAW_JSON_properties();
$DBQueryObj = new DBQuery($host, $username, $password, $database_name);

//echo 'try ' . $MagicInputObj->getJsonString();exit();

$kod_aktiviti=mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->kod_aktiviti);
$perihal_aktiviti= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->perihal_aktiviti);
$induk= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->induk);
$aktif= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->aktif);
$singkatan= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->singkatan);
$program= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->kod_program);
//$status_bahagian= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->status_bahagian);

//TODO:SEMAK JIKA KOD AKTIVITI MEMPUNYAI ANAK

$sqlQuery2 = <<<SQL
SELECT
*
FROM
  `tbl_aktiviti_ref`
WHERE `parent`='{$kod_aktiviti}'
SQL;

$DBQueryObj->setSQL_Statement($sqlQuery2);
$DBQueryObj->runSQL_Query();

if($DBQueryObj->isHavingRecordRow()){
    $has_child = 1;
}else{
  $has_child=0;
}


//TODO:UPDATE KOD AKTIVITI

$sql = <<<SQL
UPDATE
  `tbl_aktiviti_ref`
SET
  `kod_aktiviti` = '{$kod_aktiviti}',
  `perihal_aktiviti` = '{$perihal_aktiviti}',
  `parent` = '{$induk}',
  `aktif` = '{$aktif}',
  `has_child` = '{$has_child}',
  singkatan = '{$singkatan}',
  kod_program = '$program'
WHERE `kod_aktiviti` = '{$kod_aktiviti}';
SQL;

$DBCmd = new DBCommand($DBQueryObj);
$DBCmd->executeCustomQueryCommand($sql);

/* Check if command is successfull */
if ($DBCmd->getExecutionStatus() === true) {
    /* Retrieving affected row count on update */
    $rowCount = $DBCmd->getAffectedRowCount();
    //echo "Successfully! $rowCount records updated!";
    echo "Successfully updated!";
} else {
    $error_no = $DBCmd->getErrno();
    $error_message = $DBCmd->getError();
    echo "$error_no: $error_message";
}

//TODO:KEMASKINI INDUK JIKA VALUE BUKAN 0 ATAU NULL

if($induk!= '0' || $induk != '' ){

$sqlQuery2 = <<<SQL
SELECT
*
FROM
  `tbl_aktiviti_ref`
WHERE `parent`='{$induk}'
SQL;

$DBQueryObj->setSQL_Statement($sqlQuery2);
$DBQueryObj->runSQL_Query();

if($DBQueryObj->isHavingRecordRow()){
    $has_child = 1;
}else{
  $has_child=0;
}

//TODO:UPDATE INDUK

$sql = <<<SQL
UPDATE
  `tbl_aktiviti_ref`
SET
  `has_child` = '{$has_child}'
WHERE `kod_aktiviti` = '{$induk}';
SQL;

$DBCmd = new DBCommand($DBQueryObj);
$DBCmd->executeCustomQueryCommand($sql);

/* Check if command is successfull */
if ($DBCmd->getExecutionStatus() === true) {
    /* Retrieving affected row count on update */
    $rowCount = $DBCmd->getAffectedRowCount();
    //echo "Successfully! $rowCount records updated!";
    echo "Successfully updated!";
} else {
    $error_no = $DBCmd->getErrno();
    $error_message = $DBCmd->getError();
    echo "$error_no: $error_message";
}

}

//TODO: KEMASKINI PARENT JIKA STATUS TAK AKTIF

if($aktif==0){

 $sql = <<<SQL
UPDATE
  `tbl_aktiviti_ref`
SET
  `parent` =  NULL
WHERE `kod_aktiviti` = '{$kod_aktiviti}';
SQL;

$DBCmd = new DBCommand($DBQueryObj);
$DBCmd->executeCustomQueryCommand($sql);

/* Check if command is successfull */
if ($DBCmd->getExecutionStatus() === true) {
    /* Retrieving affected row count on update */
    $rowCount = $DBCmd->getAffectedRowCount();
    //echo "Successfully! $rowCount records updated!";
    echo "Successfully updated!";
} else {
    $error_no = $DBCmd->getErrno();
    $error_message = $DBCmd->getError();
    echo "$error_no: $error_message";
}
}

