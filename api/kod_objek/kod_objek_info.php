<?php
header("Access-Control-Allow-Origin: *");
include_once '../../vendor/autoload.php';
include_once '../config/db_connection.php';
include_once './KodObjekPaging.php';

const PAGING_SIZE=10;
$condition='';

$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

$GET_Data=new MagicInput();
$GET_Data->copy_GET_properties();

/**TODO: Filter **/
if(!is_null($GET_Data->kod_objek)  && $GET_Data->kod_objek!==''){
    $kod_objek= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->kod_objek);
    $condition.="WHERE kod_objek LIKE '%$kod_objek%'";    
}

if(!is_null($GET_Data->perihal_objek) && $GET_Data->perihal_objek!==''){
    $perihal_objek= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->perihal_objek);
    
    if($condition!=''){
        $condition.=" AND perihal_objek LIKE '%$perihal_objek%'";
    }else{
        $condition.="WHERE perihal_objek LIKE '%$perihal_objek%'";
    }    
}

if(!is_null($GET_Data->index_kumpulan_objek) && $GET_Data->index_kumpulan_objek!==''){
    $index_kumpulan_objek= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->index_kumpulan_objek);
    
    if($condition!=''){
        $condition.=" AND index_kumpulan_objek = $index_kumpulan_objek";
    }else{
        $condition.="WHERE index_kumpulan_objek = $index_kumpulan_objek";
    }    
}

if(!is_null($GET_Data->aktif) && $GET_Data->aktif!==''){
    $aktif= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->aktif);
    
    if($condition!=''){
        $condition.=" AND aktif = $aktif";
    }else{
        $condition.="WHERE aktif = $aktif";
    }    
}

$sql=<<<SQL
SELECT
  `kod_objek`,
  `perihal_objek`,
  `index_kumpulan_objek`,
  aktif
FROM
  `tbl_objek_ref`
$condition
SQL;
//echo $sql;exit;
$KodObjekPagingObj = new KodObjekPaging($DBQueryObj);
$KodObjekPagingObj->setSQLStatement($sql);
$KodObjekPagingObj->setPagingProperty(IPagingType::MANUAL, PAGING_SIZE);

echo json_encode($KodObjekPagingObj->getPagingInfo());