<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,Authorization');

/*TODO: (1) include all security headers above*/

include_once '../../vendor/autoload.php';

/*TODO: (2) Include EV Session Container Class*/
include_once '../login/EVSessionHandler.php';

include_once '../config/db_connection.php';

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    echo 'preflight';exit;
}

$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

/*TODO: (3) Read authentication token from front-end request*/
$headers = apache_request_headers();

/*TODO: (4) If app server capture any request header, proceed with authentication*/
if($headers){
    /*TODO: (5) Read header authorization from api request and set as session id*/
    $session_id= mysqli_real_escape_string($DBQueryObj->getLink(), $headers['Authorization']);
    
    $sessionHandler=new EVSessionHandler($DBQueryObj);
    session_set_save_handler($sessionHandler, true);
    session_id($session_id);
    session_start();
    
    if(!isset($_SESSION['icno'])){
        /*TODO: (6) Authentication failed, user is not in session*/
        header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
        echo 'Sesi tidak sah!';
        exit();
    }else{
        if(!in_array($_SESSION['roleID'], [2,3,4,5,6,7,11])){
            /*TODO: (6) Authorization failed, user is in session but lack of required access role*/
            header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
            echo 'Peranan tidak sah';
            exit();
        }
    }
}else{
    /*TODO: No header sent by requester or app server failed reading request header*/
    header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
    echo 'Sesi tidak sah!';
    exit();
}

/*TODO: (7) Authentication & Authorization is successfull, proceed with api logic*/

$MagicInputObj = new MagicInput();
$MagicInputObj->copy_RAW_JSON_properties();

$index_tetapan=$MagicInputObj->index_sesi_pb;
$keterangan= $MagicInputObj->keterangan;
$tarikh_buka=$MagicInputObj->tarikh_buka_mohon;
$tarikh_tutup= $MagicInputObj->tarikh_tutup_mohon;
$status_sesi=$MagicInputObj->status_sesi;
$status_mohon=$MagicInputObj->status_mohon;

if($status_sesi==0){
 $status_mohon=0;
}

$sql = <<<SQL
UPDATE
  `tbl_tetapan_pb`
SET
  `keterangan` = '{$keterangan}',
  `tarikh_buka_mohon` = '{$tarikh_buka}',
  `tarikh_tutup_mohon` = '{$tarikh_tutup}',
  `status_sesi` = $status_sesi,
  `status_mohon` = $status_mohon
WHERE `index_sesi_pb` = {$index_tetapan};

SQL;

//echo $sql;exit;

$DBCmd = new DBCommand($DBQueryObj);
$DBCmd->executeCustomQueryCommand($sql);

/* Check if command is successfull */
if ($DBCmd->getExecutionStatus() === true) {
    /* Retrieving affected row count on update */
    $rowCount = $DBCmd->getAffectedRowCount();
    echo "Successfully! $rowCount records updated!";
} else {
    $error_no = $DBCmd->getErrno();
    $error_message = $DBCmd->getError();
    echo "$error_no: $error_message";
}
