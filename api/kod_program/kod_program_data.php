<?php
header("Access-Control-Allow-Origin: *");
include_once '../../vendor/autoload.php';
include_once '../config/db_connection.php';
include_once './KodProgramPaging.php';

$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

const PAGING_SIZE=10;
$setCurrentPage=1;
$condition='';

$GET_Data=new MagicInput();
$GET_Data->copy_GET_properties();

if(!is_null($GET_Data->currentPage)){
    $setCurrentPage=$GET_Data->currentPage;
}

/**TODO: Filter **/
if(!is_null($GET_Data->kod_program)  && $GET_Data->kod_program!==''){
    $kod_program= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->kod_program);
    $condition.="WHERE kod_program LIKE '%$kod_program%'";    
}

if(!is_null($GET_Data->perihal_program) && $GET_Data->perihal_program!==''){
    $perihal_program= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->perihal_program);
    
    if($condition!=''){
        $condition.=" AND perihal_program LIKE '%$perihal_program%'";
    }else{
        $condition.="WHERE perihal_program LIKE '%$perihal_program%'";
    }    
}

if(!is_null($GET_Data->aktif) && $GET_Data->aktif!==''){
    $aktif= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->aktif);
    
    if($condition!=''){
        $condition.=" AND aktif = '$aktif'";
    }else{
        $condition.="WHERE aktif = '$aktif'";
    }    
}

$sql=<<<SQL
SELECT
  `kod_program`,
  `perihal_program`,
  `aktif`
FROM
  `tbl_program_ref`
$condition
SQL;
//sleep(3);
$KodProgramPagingObj = new KodProgramPaging($DBQueryObj);
$KodProgramPagingObj->setSQLStatement($sql);
$KodProgramPagingObj->setPagingProperty(IPagingType::MANUAL, PAGING_SIZE);
$KodProgramPagingObj->setPageProperty($KodProgramPagingObj->getPagingInfo());
$KodProgramPagingObj->startPaging($setCurrentPage);
