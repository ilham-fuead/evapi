<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,Authorization');

/*TODO: (1) include all security headers above*/

include_once '../../vendor/autoload.php';

/*TODO: (2) Include EV Session Container Class*/
include_once '../login/EVSessionHandler.php';

include_once '../config/db_connection.php';
include_once './item_permohonan_Paging.php';

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    echo 'preflight';exit;
}

$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

/*TODO: (3) Read authentication token from front-end request*/
$headers = apache_request_headers();

/*TODO: (4) If app server capture any request header, proceed with authentication*/
if($headers){
    /*TODO: (5) Read header authorization from api request and set as session id*/
    $session_id= mysqli_real_escape_string($DBQueryObj->getLink(), $headers['Authorization']);
    
    $sessionHandler=new EVSessionHandler($DBQueryObj);
    session_set_save_handler($sessionHandler, true);
    session_id($session_id);
    session_start();
    
    if(!isset($_SESSION['icno'])){
        /*TODO: (6) Authentication failed, user is not in session*/
        header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
        echo 'Sesi tidak sah!';
        exit();
    }else{
        if($_SESSION['roleID']<2){
            /*TODO: (6) Authorization failed, user is in session but lack of required access role*/
            header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
            echo 'Peranan tidak sah';
            exit();
        }
    }
}else{
    /*TODO: No header sent by requester or app server failed reading request header*/
    header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
    echo 'Sesi tidak sah!';
    exit();
}

/*TODO: (7) Authentication & Authorization is successfull, proceed with api logic*/

const PAGING_SIZE=10;
$setCurrentPage=1;
$condition='';

$GET_Data=new MagicInput();
$GET_Data->copy_GET_properties();

// if(!is_null($GET_Data->currentPage)){
//     $setCurrentPage=$GET_Data->currentPage;
// }

/**TODO: Filter **/
if(!is_null($GET_Data->index_permohonan)  && $GET_Data->index_permohonan!==''){
    $index_permohonan= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->index_permohonan);
    $condition.="WHERE index_permohonan = '$index_permohonan' AND kategori_simpanan_data=2";  
}
  

$sql=<<<SQL
SELECT
    `index_item`,
  `index_permohonan`,
  `kod_objek_sumber`,
  `nilai_baki_asal_objek_sumber`,
  `nilai_pindahan_objek_sumber`,
  `kod_objek_destinasi`,
  `nilai_baki_asal_objek_destinasi`,
  `nilai_terimaan_objek_destinasi`,
  `kategori_simpanan_data` 
FROM
  `tbl_item_permohonan`
$condition
SQL;
//echo $sql;exit;
//sleep(3);
$KodProgramPagingObj = new itemPermohonanPaging($DBQueryObj);
$KodProgramPagingObj->setSQLStatement($sql);
$KodProgramPagingObj->setPagingProperty(IPagingType::MANUAL, PAGING_SIZE);
$KodProgramPagingObj->setPageProperty($KodProgramPagingObj->getPagingInfo());
$KodProgramPagingObj->startPaging($setCurrentPage);
