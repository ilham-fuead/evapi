<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,Authorization');

/*TODO: (1) include all security headers above*/

include_once '../../vendor/autoload.php';

/*TODO: (2) Include EV Session Container Class*/
include_once '../login/EVSessionHandler.php';

include_once '../config/db_connection.php';
include_once './item_permohonan_Paging.php';

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    echo 'preflight';exit;
}

$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

/*TODO: (3) Read authentication token from front-end request*/
$headers = apache_request_headers();

/*TODO: (4) If app server capture any request header, proceed with authentication*/
if($headers){
    /*TODO: (5) Read header authorization from api request and set as session id*/
    $session_id= mysqli_real_escape_string($DBQueryObj->getLink(), $headers['Authorization']);
    
    $sessionHandler=new EVSessionHandler($DBQueryObj);
    session_set_save_handler($sessionHandler, true);
    session_id($session_id);
    session_start();
    
    if(!isset($_SESSION['icno'])){
        /*TODO: (6) Authentication failed, user is not in session*/
        header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
        echo 'Sesi tidak sah!';
        exit();
    }else{
        if($_SESSION['roleID']<2){
            /*TODO: (6) Authorization failed, user is in session but lack of required access role*/
            header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
            echo 'Peranan tidak sah';
            exit();
        }
    }
}else{
    /*TODO: No header sent by requester or app server failed reading request header*/
    header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
    echo 'Sesi tidak sah!';
    exit();
}

/*TODO:Get PB current ID */
$index_sesi_pb='';

$sqlCurrentPB=<<<SQL
SELECT
  `index_sesi_pb`  
FROM
  `tbl_tetapan_pb`
WHERE status_sesi=1
SQL;

    $DBQueryObj->setSQL_Statement($sqlCurrentPB);

    $DBQueryObj->runSQL_Query();

    if($DBQueryObj->isHavingRecordRow()){
        while($row=$DBQueryObj->fetchRow()){
            /* Manipulating array $row here */
            $index_sesi_pb=$row['index_sesi_pb'];
        }
    }else{
        header("{$_SERVER['SERVER_PROTOCOL']} 503 Locked");
        echo 'Tiada Prestasi Belanja yang aktif!';
        exit();
    }



/*TODO: (7) Authentication & Authorization is successfull, proceed with api logic*/

const KOD_PERAKUAN_DISYORKAN = 10;
const PAGING_SIZE=10;
$setCurrentPage=1;
$condition='';

$GET_Data=new MagicInput();
$GET_Data->copy_GET_properties();

if(!is_null($GET_Data->currentPage)){
    $setCurrentPage=$GET_Data->currentPage;
}

if(!is_null($GET_Data->status_permohonan)  && $GET_Data->status_permohonan!==''){
    $status_permohonan=implode(',', $GET_Data->status_permohonan);
    $condition.="WHERE status_permohonan IN ({$status_permohonan})";     
}else{
    $condition.="WHERE status_permohonan  =" . KOD_PERAKUAN_DISYORKAN; 
}

/*TODO: Future filter*/
if(!is_null($GET_Data->id_program) && $GET_Data->id_program!==''){
    $id_program= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->id_program);
    
    if($condition!=''){
        $condition.=" AND id_program = '$id_program'";
    }else{
        $condition.="WHERE id_program = '$id_program'";
    }
    
}

if(!is_null($GET_Data->id_aktivity) && $GET_Data->id_aktivity!==''){
    $id_aktivity= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->id_aktivity);
    
    if($condition!=''){
        $condition.=" AND id_aktivity = '$id_aktivity'";
    }else{
        $condition.="WHERE id_aktivity = '$id_aktivity'";
    }
    
}

if($condition!=''){
        $condition.=" AND index_sesi_pb= '$index_sesi_pb'";
    }else{
        $condition.="WHERE index_sesi_pb = '$index_sesi_pb'";
    } 
 
 

$sql=<<<SQL
SELECT
  `index_permohonan`,
  `id_program`,
  (SELECT perihal_program FROM tbl_program_ref WHERE kod_program=p.id_program) AS`nama_program`,
  `id_aktivity`,
  (SELECT perihal_aktiviti FROM tbl_aktiviti_ref WHERE kod_aktiviti=p.id_aktivity)`nama_aktiviti`,
  DATE_FORMAT(tarikh_permohonan, "%d/%m/%Y") as `tarikh_permohonan`,
  `id_pegawai_memohon`,
  `tarikh_proses`,
  `id_pegawai_proses`,
  DATE_FORMAT(tarikh_syor, "%d/%m/%Y") as `tarikh_syor`,
  `id_pegawai_syor`,
  `tarikh_peraku`,
  `id_pegawai_peraku`,
  `tarikh_peraku_kedua`,
  `id_pegawai_peraku_kedua`,
  `status_permohonan`,
  (SELECT perihal_status_permohonan FROM tbl_status_permohonan_ref WHERE id_status_permohonan=p.status_permohonan) AS`perihal_status_permohonan`,
  `justifikasi_permohonan`,
  `no_rujukan_permohonan`,
  `justifikasi_pindaan` as justifikasi_penyedia 
FROM
  `tbl_permohonan` p
$condition
SQL;
//echo $sql;exit;

$PerakuanListBaharuPagingObj = new itemPermohonanPaging($DBQueryObj);
$PerakuanListBaharuPagingObj->setSQLStatement($sql);
$PerakuanListBaharuPagingObj->setPagingProperty(IPagingType::MANUAL, PAGING_SIZE);
$PerakuanListBaharuPagingObj->setPageProperty($PerakuanListBaharuPagingObj->getPagingInfo());
$PerakuanListBaharuPagingObj->startPaging($setCurrentPage);
