<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,Authorization');

/*TODO: (1) include all security headers above*/

include_once '../../vendor/autoload.php';

/*TODO: (2) Include EV Session Container Class*/
include_once '../login/EVSessionHandler.php';

include_once '../config/db_connection.php';

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    echo 'preflight';exit;
}

$status_permohonan=1;
$today = date("Y-m-d");

$DBQueryObj = new DBQuery($host, $username, $password, $database_name);

/*TODO: (3) Read authentication token from front-end request*/
$headers = apache_request_headers();

/*TODO: (4) If app server capture any request header, proceed with authentication*/
if($headers){
    /*TODO: (5) Read header authorization from api request and set as session id*/
    $session_id= mysqli_real_escape_string($DBQueryObj->getLink(), $headers['Authorization']);
    
    $sessionHandler=new EVSessionHandler($DBQueryObj);
    session_set_save_handler($sessionHandler, true);
    session_id($session_id);
    session_start();
    
    if(!isset($_SESSION['icno'])){
        /*TODO: (6) Authentication failed, user is not in session*/
        header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
        echo 'Sesi tidak sah!';
        exit();
    }else{
        if(!in_array($_SESSION['roleID'], [2,3,4,5,11])){
            /*TODO: (6) Authorization failed, user is in session but lack of required access role*/
            header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
            echo 'Peranan tidak sah';
            exit();
        }
    }
}else{
    /*TODO: No header sent by requester or app server failed reading request header*/
    header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
    echo 'Sesi tidak sah!';
    exit();
}



/*TODO:Get PB current ID */
$index_sesi_pb='';

$sqlCurrentPB=<<<SQL
SELECT
  `index_sesi_pb`  
FROM
  `tbl_tetapan_pb`
WHERE status_sesi=1
SQL;

    $DBQueryObj->setSQL_Statement($sqlCurrentPB);

    $DBQueryObj->runSQL_Query();

    if($DBQueryObj->isHavingRecordRow()){
        while($row=$DBQueryObj->fetchRow()){
            /* Manipulating array $row here */
            $index_sesi_pb=$row['index_sesi_pb'];
        }
    }else{
        header("{$_SERVER['SERVER_PROTOCOL']} 503 Locked");
        echo 'Tiada Prestasi Belanja yang aktif!';
        exit();
    }

/*TODO: (7) Authentication & Authorization is successfull, proceed with api logic*/


$MagicInputObj = new MagicInput();
$MagicInputObj->copy_RAW_JSON_properties();


$id_aktivity= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->id_aktiviti);
$id_program= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->id_program);
$justifikasi_permohonan= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->justifikasi);
//$id_pegawai_memohon= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->id_pegawai_memohon);
$id_pegawai_memohon=$_SESSION['icno'];
$userID=$_SESSION['userID']; 


$sql = <<<SQL
INSERT INTO `tbl_permohonan` (
  `id_program`,
  `id_aktivity`,
  `tarikh_permohonan`,
  `id_pegawai_memohon`,
  `status_permohonan`,
  `justifikasi_permohonan`,
  index_sesi_pb 
)
VALUES
  (
    '{$id_program}',
    '{$id_aktivity}',
    '{$today}',
    '{$id_pegawai_memohon}',
    $status_permohonan,
    '{$justifikasi_permohonan}',
    $index_sesi_pb
  );
SQL;

//echo $sql;exit;

$DBCmd = new DBCommand($DBQueryObj);
$DBCmd->executeCustomQueryCommand($sql);

/* Check if command is successfull */
if ($DBCmd->getExecutionStatus() === true) {
    /* Retrieving affected row count on update */
    $rowCount = $DBCmd->getAffectedRowCount();
    $getinsertID = $DBCmd->getinsertID();
    //echo "Successfully! $rowCount records updated!";
    $obj = new MagicObject();
    $obj->status = $DBCmd->getAffectedRowCount();
    $obj->getinsertID = $DBCmd->getinsertID();
    $obj->userID = $userID;
    $obj->errorMessage = 'Berjaya';
    echo $obj->getJsonString();
} else {
    $error_no = $DBCmd->getErrno();
    $error_message = $DBCmd->getError();
    echo "$error_no: $error_message";
}






















?>
