<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,Authorization'); 
include_once '../../vendor/autoload.php';
include_once '../config/db_connection.php';
include_once './Pengguna_paging.php';

$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

const PAGING_SIZE=10;
$setCurrentPage=1;
$condition='';

$GET_Data=new MagicInput();
$GET_Data->copy_GET_properties();

if(!is_null($GET_Data->currentPage)){
    $setCurrentPage=$GET_Data->currentPage;
}

if(!is_null($GET_Data->nama)  && $GET_Data->nama!==''){
    $nama= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->nama);
    $condition.="AND Nama LIKE '%$nama%'";
    
}

if(!is_null($GET_Data->nokp) && $GET_Data->nokp!==''){
    $nokp= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->nokp);
    
     if($condition!=''){
        $condition.=" AND NoKP = '$nokp'";
    }else{
        $condition.="AND NoKP = '$nokp'";
    }
}  
if(!is_null($GET_Data->kod_aktiviti) && $GET_Data->kod_aktiviti!==''){
    $kod_aktiviti= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->kod_aktiviti);
    
    if($condition!=''){
        $condition.=" AND kod_aktiviti = '$kod_aktiviti'";
    }else{
        $condition.="AND kod_aktiviti = '$kod_aktiviti'";
    }
    
}

if(!is_null($GET_Data->peranan) && $GET_Data->peranan!==''){
    $peranan= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->peranan);
    
    if($condition!=''){
        $condition.=" AND roleID = '$peranan'";
    }else{
        $condition.="AND roleID = '$peranan'";
    }
    
}  

$sql=<<<SQL
SELECT
  `NoKP`,
  `Nama`,
  `Emel`,
  `BUId`,
  `roleID`,
  `NoTelBimbit`,
  `userStatusID`,
  `kategoripenggunaID`,
  `BUID_Kementerian`,
  `BUID_Agensi`,
  `kemaskini_oleh`,
  `adminStatus`,
   NoTelBimbit,
  `KodBUOrgChart`,
  `tarikh_kemaskini`,
  `status_aktif`,
  `roleID`,
  (SELECT `nama_peranan` FROM `tbl_ref_peranan` WHERE `kod_peranan`=roleID) AS peranan,
  bahagian,
  kod_aktiviti
FROM
  `tbl_pengguna`
WHERE `BUID_Agensi` = 13280
$condition
SQL;
//sleep(3);
$PenggunaPagingObj = new PenggunaPaging($DBQueryObj);
$PenggunaPagingObj->setSQLStatement($sql);
$PenggunaPagingObj->setPagingProperty(IPagingType::MANUAL, PAGING_SIZE);
$PenggunaPagingObj->setPageProperty($PenggunaPagingObj->getPagingInfo());
$PenggunaPagingObj->startPaging($setCurrentPage);
