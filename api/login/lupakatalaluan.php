<?php
include '../../../vendor/autoload.php';
include '../../config/db_connection.php';
include '../../config/mailing.php';

$userObj = json_decode(file_get_contents("php://input"));
unset($_POST);

$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

$NoKP=mysqli_real_escape_string($DBQueryObj->getLink(),$userObj->nokp);
$Emel=mysqli_real_escape_string($DBQueryObj->getLink(),$userObj->email);

$sql='SELECT Nama, kategoripenggunaID '
    . 'FROM tblusers '
    . 'WHERE NoKP = "' . $NoKP .'" '
    . 'AND Emel = "' . $Emel .'"';

$DBQueryObj->setSQL_Statement($sql);



$DBQueryObj->runSQL_Query();

//var_dump($DBQueryObj->isHavingRecordRow());exit;
if($DBQueryObj->isHavingRecordRow()) {
    $row = mysqli_fetch_assoc($DBQueryObj->getQueryResult());
    if ($row['kategoripenggunaID'] !== 1) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $newPassword = substr(str_shuffle($chars), 0, 6); // set length to 6

        $sqlCmdObj = new DBCommand($DBQueryObj);
        $sqlCmdObj->setUPDATEtoTable('tblusers');
        $sqlCmdObj->addUPDATEcolumn('password', $newPassword, IFieldType::STRING_TYPE);
        $sqlCmdObj->addConditionStatement('NoKP', $userObj->nokp, IFieldType::INTEGER_TYPE, IConditionOperator::AND_OPERATOR);
        $sqlCmdObj->addConditionStatement('Emel', $userObj->email, IFieldType::STRING_TYPE, IConditionOperator::AND_OPERATOR);
        $sqlCmdObj->executeQueryCommand();

        /**********************************************************/
        $mail->addAddress($userObj->email, $row['Nama']);

        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = 'SPMB: Katalaluan Baru';

        $mail->Body = <<<BODY
Hai {$row['Nama']},
<br/><br/>
Anda telah memilih untuk set semula katalaluan pada sistem aplikasi <b>SPMB</b>.<br/>
Berikut adalah katalaluan baru yang telah dijana untuk anda:
<br/><br/>
<b>$newPassword</b><br/>
<br/>
Sila gunakan katalaluan tersebut untuk log masuk ke dalam sistem.<br/>
Terima kasih.
BODY;

        $mail->AltBody = <<<AltBody
Hai {$row['Nama']},

Anda telah memilih untuk set semula katalaluan pada sistem aplikasi SPMB.
Berikut adalah katalaluan baru yang telah dijana untuk anda:

$newPassword

Sila gunakan katalaluan tersebut untuk log masuk ke dalam sistem.
Terima kasih.
AltBody;

        if (!$mail->send()) {
//            echo 'Maaf. Terdapat ralat untuk menghantar emel.';
//            echo 'Ralat Emel: ' . $mail->ErrorInfo;
        } else {
//            echo 'Emel katalaluan baru telah dihantar kepada anda. Sila semak emel anda.';

        }

        echo json_encode($row);
//    echo $DBQueryObj->getRowsInJSON();
    } else {
        http_response_code(400);
    }
} else {
    http_response_code(400);
}