<?php
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class EV_Security_Soft extends Login {

    public $loggedInUser;
    public $session_id;
    const BUID_JPA=13280;

    /**
     * To set up proper redirection file
     *
     * An method that must be implemented in order to set up proper
     * redirection to support security feature in User class.
     *
     * Utility Method for setting up redirection file (use $this-> to invoke):<br>
     * i.   setAfterLogInPage<br>
     * ii.  setAfterLogoutInPage<br>
     * iii. setNotInSessionPage<br>
     * iv.  setNoAuthorizationForModulPage<br>
     * v.   setInvalidRightRedirectPage<br>
     *
     */
    protected function setRedirectFiles() {
        // TODO: Implement setRedirectFiles() method.
    }
    
    public function loadUserDetail($uName){
        

$sql=<<<SQL
SELECT
  `userID`,
  `NoKP`,
  `session_id`
FROM
  `tbl_pengguna`
WHERE 
`NoKP`='$uName'
AND (session_id IS NOT NULL AND session_id<>'')
SQL;

        //echo $sql;exit();
        $this->dbQueryObj->setSQL_Statement($sql);
        $this->dbQueryObj->runSQL_Query();

        if($this->dbQueryObj->isHavingRecordRow()){
            $row = $this->dbQueryObj->fetchRow();
        
            $this->session_id=$row['session_id'];
        }else{
            $this->session_id='invalid';
        }
        //echo $this->session_id;exit;
    }

    public function executeLogin($uName, $uPassword) {
        
        $authorizationObj=new MagicObject();
        $authorizationObj->session_id='invalid';
        
        if ($this->isAuthenticated($uName, $uPassword)) {
            //echo '';
            $this->setAuthenticate($uName);
            $this->loadUserDetail($uName);
            $authorizationObj->session_id= $this->session_id;
            
            if ($this->httpResponseAction == IAuthenticationAction::REDIRECT) {
                $this->redirect("in");
            } else if ($this->httpResponseAction == IAuthenticationAction::SET_HTTP_RESPONSE_HEADER) {
                header("{$_SERVER['SERVER_PROTOCOL']} 200 OK");
                echo $authorizationObj->getJsonString();
                exit;
            }
        } else {
            header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
            echo $authorizationObj->getJsonString();
            exit;
        }
    }

    protected function isAuthenticated($usename, $password) {
        $credObj = new MagicObject();
        $credObj->icno = $usename;
        $credObj->password = $password;

        /* TODO : Check user already register with spmb */

        $sql = <<<SQL
Select userID,kategoripenggunaID 
FROM tbl_pengguna 
WHERE NoKP='$usename'
SQL;

        $this->dbQueryObj->setSQL_Statement($sql);
        $this->dbQueryObj->runSQL_Query();

        if ($this->dbQueryObj->isHavingRecordRow()) {
            $row = $this->dbQueryObj->fetchRow();

            if ($row['kategoripenggunaID'] == 1) {
                /* Login using HRMIS credential */
                return $this->loginHRMIS($credObj);
            } else {
                /* Login using SPMB internal credential */
                return $this->loginSPMB($credObj);
            }
        }
    }    

    public function loginSPMB($credObj, $doRegister = false) {

        $loggedInUser = json_decode('{"id":"","name":"","icno":"","emel":""}');

        if ($credObj->icno && $credObj->password) {

$sql = <<<SQL
SELECT NoKP,password,confirmation
FROM tbl_pengguna
WHERE NoKP='{$credObj->icno}' AND password='{$credObj->password}'
SQL;

            $this->dbQueryObj->setSQL_Statement($sql);
            $this->dbQueryObj->runSQL_Query();

            if ($this->dbQueryObj->isHavingRecordRow()) {
               return true;
            } else {
               return false;
            }
        } else {
            return false;
        }
    }

    public function loginHRMIS($credObj, $doRegister = false) {

        $loggedInUser = json_decode('{"id":"","name":"","icno":"","emel":""}');

        if ($credObj->icno && $credObj->password) {
            $client = new Client();

            try {
                /*
                 * hrmis mobile LIVE : https://mobile.eghrmis.gov.my/hrmis_rest/spmb/login
                 * pre-hrmis login for SPMB : http://10.21.0.75/hrmis_rest_spmb/spmb/login
                 * hrmis login baru latest pre 27102022 : 'http://10.21.153.183/hrmis_rest_pre/spmb/login'
                 */
                $response = $client->request('POST', 'http://10.21.153.183/hrmis_rest_pre/spmb/login', [
                    'form_params' => [
                        'icno' => $credObj->icno,
                        'password' => $credObj->password
                    ],
                    'headers' => [
                        'Authorization' => 'getHrmisData eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiYWRtaW4iLCJyb2xlcyI6WyJhZG1pbmlzdHJhdG9ycyIsImNhbl9lZGl0X2VudGl0eSIsImNhbl9lZGl0X3dvcmtmbG93IiwiZGV2ZWxvcGVycyJdLCJlbWFpbCI6MTU0ODg5MjgwMCwic3ViIjoiNzQxNmEyOTQtZDMwMy00YTA4LWEzNDktNmIwMmU0ZTJlNjgyIiwibmJmIjoxNDc2OTMyMjY2LCJpYXQiOjE0NjExMjEwNjYsImV4cCI6MTU0ODg5MjgwMCwiYXVkIjoicnhnZW5lcmljIn0.hMq4Sltyy_24DWzVD8rJHJYPgG7Emp5EdCBrESLlNOk'
                    ],
                    'timeout' => 10,
                    'http_errors' => false
                ]);

                $body = (string) $response->getBody();

                /* TODO : Retrieved status code */
                $statusCode = $response->getStatusCode();

                if ($statusCode == 200) {

                    /* TODO : Passed JSON from HRMIS server as loggedInUser obj */
                    $loggedInUser = json_decode($body);

                    if (isset($loggedInUser) && !$loggedInUser->error) {
                        /* TODO : loggedInUser obj doesn't have error prop (Successful response) */                        
                        
                        if ($loggedInUser->BUID_Agensi!='13280') {                                
                                return false;
                        }                    
                    } else {                        
                        return false;
                    }
                } else {
                    return false;
                }

                return true;
            } catch (RequestException $e) {
                //errno 20 = timeout
                return false;
            } catch (\Exception $e) {
                return false;
            }
        } else {
            return false;
        }
    }

    private function is_HRMIS_user($icno) {

        $HrmisUser = json_decode('{"status":"TIDAK"}');

        $client = new Client();

        try {
            // REST API (TRAINING) : http://10.21.0.75/hrmis_rest/spmb/semak/$icno
            // REST API (LIVE) : https://mobile.eghrmis.gov.my/hrmis_rest/spmb/semak/$icno
            $response = $client->request('GET', "https://mobile.eghrmis.gov.my/hrmis_rest/spmb/semak/$icno", [
                'headers' => [
                    'Authorization' => 'getHrmisData eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiYWRtaW4iLCJyb2xlcyI6WyJhZG1pbmlzdHJhdG9ycyIsImNhbl9lZGl0X2VudGl0eSIsImNhbl9lZGl0X3dvcmtmbG93IiwiZGV2ZWxvcGVycyJdLCJlbWFpbCI6MTU0ODg5MjgwMCwic3ViIjoiNzQxNmEyOTQtZDMwMy00YTA4LWEzNDktNmIwMmU0ZTJlNjgyIiwibmJmIjoxNDc2OTMyMjY2LCJpYXQiOjE0NjExMjEwNjYsImV4cCI6MTU0ODg5MjgwMCwiYXVkIjoicnhnZW5lcmljIn0.hMq4Sltyy_24DWzVD8rJHJYPgG7Emp5EdCBrESLlNOk'
                ],
                'timeout' => 10,
                'http_errors' => false
            ]);

            $body = (string) $response->getBody();

            /* TODO : Retrieved status code */
            $statusCode = $response->getStatusCode();

            if ($statusCode == 200) {

                /* TODO : Passed JSON from HRMIS server as HrmisUser obj */
                $HrmisUser = json_decode($body);

                if ($HrmisUser->status == 'YA') {
                    /* TODO : Is valid HRMIS user but login failed */
                    return true;
                }else{
                    /* TODO : Is invalid HRMIS user */
                    return false;
                }
            } else {
                return false;
            }

            
        } catch (RequestException $e) {
            //errno 20 = timeout
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }       
    
    public function removeUserDetail($key){
        $this->deleteUserDetail($key);
    }
    
    public function newUserDetail($key, $value){
        $this->addUserDetail($key, $value);
    }

    private function getJSONifiedLastError(){
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                return ' - No errors';
            break;
            case JSON_ERROR_DEPTH:
                return ' - Maximum stack depth exceeded';
            break;
            case JSON_ERROR_STATE_MISMATCH:
                return ' - Underflow or the modes mismatch';
            break;
            case JSON_ERROR_CTRL_CHAR:
                return ' - Unexpected control character found';
            break;
            case JSON_ERROR_SYNTAX:
                return ' - Syntax error, malformed JSON';
            break;
            case JSON_ERROR_UTF8:
                return ' - Malformed UTF-8 characters, possibly incorrectly encoded';
            break;
            default:
                return ' - Unknown JSON error';
            break;
        }
    }


}
