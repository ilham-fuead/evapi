<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
//header('Access-Control-Allow-Headers: *');
header('Access-Control-Allow-Headers: Content-Type,Authorization');

include_once '../../vendor/autoload.php';
include_once '../config/db_connection.php';
include_once './EVSessionHandler.php';

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    echo 'preflight';exit;
}

$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

$GET_Data=new MagicInput();
$GET_Data->copy_GET_properties();

$condition='';

$headers = apache_request_headers();

//echo var_dump($headers['Authorization']);
//exit;

/**TODO: Filter **/
if($headers){
    $session_id= mysqli_real_escape_string($DBQueryObj->getLink(), $headers['Authorization']);
    
    $sessionHandler=new EVSessionHandler($DBQueryObj);
    session_set_save_handler($sessionHandler, true);
    session_id($session_id);
    session_start();
    
    var_dump($_SESSION);
}else{
    echo '{error:"no client sessionid"}';
}

/**TODO: Prototype 
if(!is_null($GET_Data->session_id)  && $GET_Data->session_id!==''){
    $session_id= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->session_id);
    
    $sessionHandler=new EVSessionHandler($DBQueryObj);
    session_set_save_handler($sessionHandler, true);
    session_id($session_id);
    session_start();
    
    var_dump($_SESSION);
}else{
    echo '{error:"no client sessionid"}';
}
**/
