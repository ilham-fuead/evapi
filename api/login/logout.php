<?php
/**
 * Created by PhpStorm.
 * User: JPA
 * Date: 9/11/2016
 * Time: 3:23 PM
 */
require_once '../../../vendor/autoload.php';
require_once '../../config/db_connection.php';
require_once 'SPMB_Security.php';

$DBQueryObj=new DBQuery($host,$username,$password,$database_name);

$loginObj=new SPMB_Security($DBQueryObj,IAuthenticationAction::SET_HTTP_RESPONSE_HEADER);

session_regenerate_id(true);
session_unset();
session_destroy();
session_write_close();
setcookie(session_name(), '', 0, '/');

