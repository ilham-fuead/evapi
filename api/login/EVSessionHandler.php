<?php

class EVSessionHandler implements SessionHandlerInterface {

    private $connection;
    private $DBQuery;
    public $sessionID;

    public function __construct(DBQuery $DBQueryObj) {
        $this->DBQuery=$DBQueryObj;
        $connDetails = $DBQueryObj->getConnectionDetail();
        $this->connection = new mysqli($connDetails->host, $connDetails->username, $connDetails->password, $connDetails->database_name);
    }

    public function open($savePath, $sessionName) {
        if ($this->connection) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function read($sessionId) {
        try {
            $stmt = $this->connection->prepare("SELECT session_data FROM sessions WHERE session_id = ?");
            $stmt->bind_param("s", $sessionId);
            $stmt->execute();
            $stmt->bind_result($sessionData);
            $stmt->fetch();
            $stmt->close();

            return $sessionData ? $sessionData : '';
        } catch (Exception $e) {
            return '';
        }
    }

    public function write($sessionId, $sessionData) {
        try {
            $stmt = $this->connection->prepare("REPLACE INTO sessions(`session_id`, `created`, `session_data`, `tarikh`) VALUES(?, ?, ?, ?)");
            $time = time();
            $today = date("Y-m-d H:i:s");
            $stmt->bind_param("siss", $sessionId, $time, $sessionData, $today);
            $stmt->execute();
            $stmt->close();
            
            if(strlen($sessionData)>0){
                $this->updateTblUser($sessionId,$sessionData);
            }            

            return TRUE;
        } catch (Exception $e) {
            return FALSE;
        }
    }

    public function destroy($sessionId) {
        try {
            $stmt = $this->connection->prepare("DELETE FROM sessions WHERE session_id = ?");
            $stmt->bind_param("s", $sessionId);
            $stmt->execute();
            $stmt->close();

            return TRUE;
        } catch (Exception $e) {
            return FALSE;
        }
    }

    public function gc($maxlifetime) {
        $past = time() - $maxlifetime;

        try {
            $stmt = $this->connection->prepare("DELETE FROM sessions WHERE `created` < ?");
            $stmt->bind_param("i", $past);
            $stmt->execute();
            $stmt->close();

            return TRUE;
        } catch (Exception $e) {
            return FALSE;
        }
    }

    public function loadSessionData($sessionId) {
        $sessionDataString = $this->read($sessionId);
        $sessionArr = [];

        if (strlen($sessionDataString) > 0) {
            $sessionArr = explode(';', $sessionDataString);
            foreach ($sessionArr as $value) {
                $valueArr = explode('|', $value);
                if (strlen($valueArr[0]) > 0) {
                    $_SESSION[$valueArr[0]] = $this->getValue($valueArr[1]);
                }
            }
        }
    }

    private function updateTblUser($sessionId,$sessionData) {
        
        $sessionDataString = $this->read($sessionId);
        $sessionArr = [];
        $sessionDataArray=[];

        if (strlen($sessionDataString) > 0) {
            $sessionArr = explode(';', $sessionDataString);
            foreach ($sessionArr as $value) {
                $valueArr = explode('|', $value);
                
                if(sizeof($valueArr)!=2){
                    continue;
                }
                
                if (strlen($valueArr[0]) > 0) {
                    $valuePart=$this->getValue($valueArr[1]);
                    $valuePartClean= str_replace('"','',$valuePart);
                    $sessionDataArray+=[$valueArr[0]=>$valuePartClean];
                }
            }
        }
        
        $sql="UPDATE `tbl_pengguna` SET `session_id`='$sessionId',`session_data` = '$sessionData' WHERE `NoKP` = '" . $sessionDataArray['icno'] . "'";
        
        $cmd=new DBCommand($this->DBQuery);
        $cmd->executeCustomQueryCommand($sql);  
        $this->sessionID=$sessionId;
        
//        echo 'my array:';
//        var_dump($sessionDataArray);exit();
    }

    private function getValue($value) {
        $valueArr=explode(':', $value);
        
        if(sizeof($valueArr)>2){
            return explode(':', $value)[2];
        }
            else{
              return '';  
            }
    }
    
    public function close() {
        return TRUE;
    }

}
