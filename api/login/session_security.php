<?php
$headers = apache_request_headers();

if($headers){
    $session_id= mysqli_real_escape_string($DBQueryObj->getLink(), $headers['Authorization']);
    
    $sessionHandler=new EVSessionHandler($DBQueryObj);
    session_set_save_handler($sessionHandler, true);
    session_id($session_id);
    session_start();
    
    //var_dump($_SESSION);
}else{
    header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
    exit();
}

