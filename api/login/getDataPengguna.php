<?php
include '../../../vendor/autoload.php';
include '../../config/db_connection.php';

$saveAduanObj = json_decode(file_get_contents("php://input"));
$respObj = new MagicObject();
$ICNO='';
/* Retrieve params if any */
if (isset($saveAduanObj->ICNO)) {
    $ICNO = $saveAduanObj->ICNO;
}
//echo "nokp :".$ICNO;exit;

//$ICNO='830512065661';

/* TODO: CHECK DATA DARI TABLE tblusers */
$sql = <<<SQL
SELECT * FROM tblusers WHERE NoKP = $ICNO
SQL;

$DBQueryObj = new DBQuery($host, $username, $password, $database_name);
$DBQueryObj->setSQL_Statement($sql);
$DBQueryObj->runSQL_Query();

if ($DBQueryObj->isHavingRecordRow()) {

    $respObj->status = 0;
    $respObj->errmsg = 'Rekod telah wujud. Sila log masuk.';
    echo $respObj->getJsonString();

}else {

    /* TODO : CARIAN PENGGUNA DALAM TABLE HRMIS */

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "http://10.21.0.75/hrmis_rest/spmb/personal/$ICNO",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Authorization: getHrmisData eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiYWRtaW4iLCJyb2xlcyI6WyJhZG1pbmlzdHJhdG9ycyIsImNhbl9lZGl0X2VudGl0eSIsImNhbl9lZGl0X3dvcmtmbG93IiwiZGV2ZWxvcGVycyJdLCJlbWFpbCI6MTU0ODg5MjgwMCwic3ViIjoiNzQxNmEyOTQtZDMwMy00YTA4LWEzNDktNmIwMmU0ZTJlNjgyIiwibmJmIjoxNDc2OTMyMjY2LCJpYXQiOjE0NjExMjEwNjYsImV4cCI6MTU0ODg5MjgwMCwiYXVkIjoicnhnZW5lcmljIn0.hMq4Sltyy_24DWzVD8rJHJYPgG7Emp5EdCBrESLlNOk",
            "cache-control: no-cache",
            "postman-token: 2e375075-eff3-d6c9-f644-6f210b7df016"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
//        echo "cURL Error #:" . $err;
//        echo '{"NoKP":""}';
        $respObj->status = 0;
        $respObj->errmsg = 'Maaf. Terdapat masalah capaian ke login server HRMIS. Sila cuba sebentar lagi.';
        echo $respObj->getJsonString();
    } else {

        $NoKP = json_decode($response, true);

        if (isset($NoKP['NoKP'])) {

//            echo $response;
            $respObj->status = 0;
            $respObj->errmsg = 'Tiada pendaftaran diperlukan bagi penjawat awam. Sila log masuk menggunakan ID dan katalaluan sama seperti HRMIS.';
            echo $respObj->getJsonString();

        } else {
            echo '{"NoKP":""}';
        }

    }
}
//var_dump($response);