<?php

header("Access-Control-Allow-Origin: *");
//header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Methods: *');
header('Access-Control-Allow-Headers: Content-Type,Authorization');

include_once '../../vendor/autoload.php';
include_once '../config/db_connection.php';
include_once '../login/EVSessionHandler.php';

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    echo 'preflight';
    exit;
} else {

    $DBQueryObj = new DBQuery($host, $username, $password, $database_name);

    $condition = '';

    $headers = apache_request_headers();

//    echo var_dump($headers['Authorization']);
//    exit;

    /*     * TODO: Filter * */
    if ($headers) {
        $session_id = mysqli_real_escape_string($DBQueryObj->getLink(), $headers['Authorization']);

        $sessionHandler = new EVSessionHandler($DBQueryObj);
        session_set_save_handler($sessionHandler, true);
        session_id($session_id);
        session_start();

        if(!isset($_SESSION['icno'])){
            header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
            echo 'Sesi tidak sah!';
            exit();
        }else{
            if(!in_array($_SESSION['roleID'], [2,3,4,5,11])){
                /*TODO: (6) Authorization failed, user is in session but lack of required access role*/
                header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
                echo 'Peranan tidak sah';
                exit();
            }
        }
    } else {
        header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");                
        echo '{error:"no client sessionid"}';
        exit();
    }

    $MagicInputObj = new MagicInput();
    $MagicInputObj->copy_RAW_JSON_properties();

    $index_permohonan = mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->index_permohonan);
    $statusPerakuan = mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->statusPerakuan);
    $jenisPerakuan = mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->jenisPerakuan);
    $catatan_peraku1 = mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->catatan_peraku1);
    $catatan_peraku2 = mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->catatan_peraku2);

    $errorMsg='';
    
    /*TODO:Get PB current ID */
    $index_sesi_pb='';

$sqlCurrentPB=<<<SQL
SELECT
  `index_sesi_pb`  
FROM
  `tbl_tetapan_pb`
WHERE status_sesi=1
SQL;

    $DBQueryObj->setSQL_Statement($sqlCurrentPB);

    $DBQueryObj->runSQL_Query();

    if($DBQueryObj->isHavingRecordRow()){
        while($row=$DBQueryObj->fetchRow()){
            /* Manipulating array $row here */
            $index_sesi_pb=$row['index_sesi_pb'];
        }
    }else{
        header("{$_SERVER['SERVER_PROTOCOL']} 503 Locked");
        echo 'Tiada Prestasi Belanja yang aktif!';
        exit();
    }
    
    if ($jenisPerakuan == 1) {
    /*TODO: Process 1st Perakuan*/
        
$sql = <<<SQL
UPDATE
  `tbl_permohonan`
SET
  `tarikh_peraku` = NOW(),
  `id_pegawai_peraku` = '{$_SESSION["icno"]}',
  `catatan_peraku1` = '{$catatan_peraku1}',
  `status_permohonan` = 5
WHERE `index_permohonan` = '$index_permohonan';
SQL;
  
        $DBCmd = new DBCommand(new DBQuery($host, $username, $password, $database_name));
        $DBCmd->executeCustomQueryCommand($sql);

        if ($DBCmd->getExecutionStatus() === true) {
            echo 'Permohonan Berjaya disimpan (Perakuan Pertama)!';
        }else{
            $errorMsg='Permohonan Gagal disimpan (Perakuan Pertama)!';
        }
  
    }else if ($jenisPerakuan == 2) {
    /*TODO: Process 2nd Perakuan*/    
    $allSQL='';
        
$sqlGetItemsPermohonan = <<<SQL
SELECT
  `index_item`,
  p.`index_permohonan`,
  `kod_objek_sumber`,
  `nilai_baki_asal_objek_sumber`,
  `nilai_pindahan_objek_sumber`,
  `kod_objek_destinasi`,
  `nilai_baki_asal_objek_destinasi`,
  `nilai_terimaan_objek_destinasi`,
  `kategori_simpanan_data`,
  `id_aktivity`
FROM
  `tbl_item_permohonan` i INNER JOIN `tbl_permohonan` p
  ON i.`index_permohonan`=p.`index_permohonan`
WHERE p.`index_permohonan`= '$index_permohonan';
SQL;

        $DBQueryObj->setSQL_Statement($sqlGetItemsPermohonan);
        
        $allSQL=$sqlGetItemsPermohonan;
//        echo $allSQL;exit();
        $DBQueryObj->runSQL_Query();
        
        $DBCmd = new DBCommand(new DBQuery($host, $username, $password, $database_name));
        $DBCmd->enableTransaction();
        
        if ($DBQueryObj->isHavingRecordRow()) {
            $rowsItemPermohonan=[];
            foreach ($DBQueryObj->yieldRow() as $row) {
                $rowsItemPermohonan[] = $row;
            }

//            var_dump($rows);exit();                     

            foreach ($rowsItemPermohonan as $row) {

                $ObjSrcOriginalVal = 0;
                $ObjDesOriginalVal = 0;
                $id_aktivity = $row['id_aktivity'];
                $kod_objek_sumber = $row['kod_objek_sumber'];
                $kod_objek_destinasi = $row['kod_objek_destinasi'];
                $nilai_pindahan_objek_sumber = $row['nilai_pindahan_objek_sumber'];
                $nilai_terimaan_objek_destinasi = $row['nilai_terimaan_objek_destinasi'];   

/* TODO: Get obj src original value */
$sqlObjSrcOriginalVal = <<<SQL
SELECT
  `index_PB`,
  `kod_objek`,
  `perihal_objek`,
  `peruntukan_dipinda`  
FROM
  `tbl_prestasi_belanja`
WHERE `id_aktiviti`='$id_aktivity'
AND kod_objek='$kod_objek_sumber'
AND `index_sesi_pb`='{$index_sesi_pb}'
SQL;

                $DBQueryObj->setSQL_Statement($sqlObjSrcOriginalVal);
//                $allSQL= '<OBJEK SRC ORIGINAL>';
//                $allSQL.= $sqlObjSrcOriginalVal;
//                echo $allSQL;exit();
                $DBQueryObj->runSQL_Query();
                
                if ($DBQueryObj->isHavingRecordRow()) {
                    $rows=[];
                    foreach ($DBQueryObj->yieldRow() as $row) {
                        $rows[] = $row;
                    }
                    //var_dump($rows);exit();
                    $ObjSrcOriginalVal = $rows[0]['peruntukan_dipinda'];
                }


/* TODO: Get obj destination original value */
$sqlObjDesOriginalVal = <<<SQL
SELECT
  `index_PB`,
  `kod_objek`,
  `perihal_objek`,
  `peruntukan_dipinda` 
FROM
  `tbl_prestasi_belanja`
WHERE `id_aktiviti`='$id_aktivity'
AND kod_objek='$kod_objek_destinasi'
AND `index_sesi_pb`='{$index_sesi_pb}'
SQL;

                $DBQueryObj->setSQL_Statement($sqlObjDesOriginalVal);
//                $allSQL= '<OBJEK DES ORIGINAL>';
//                $allSQL.= $sqlObjDesOriginalVal;
//                echo $allSQL;exit();
                $DBQueryObj->runSQL_Query();
                if ($DBQueryObj->isHavingRecordRow()) {
                    $rows=[];
                    foreach ($DBQueryObj->yieldRow() as $row) {
                        $rows[] = $row;
                    }

                    $ObjDesOriginalVal = $rows[0]['peruntukan_dipinda'];
                }

                /* TODO: Kemaskini sumber */

                $objSrcNewVal = $ObjSrcOriginalVal - $nilai_pindahan_objek_sumber;

$sqlUpdObjSrcVal = <<<SQL
UPDATE
  `tbl_prestasi_belanja`
SET
  `peruntukan_dipinda` = '$objSrcNewVal',
  `edit_by` = 'SISTEM - PP',
  `last_edit_datetime` = NOW()
WHERE `kod_objek` = '$kod_objek_sumber'
  AND `id_aktiviti` = '$id_aktivity'
  AND `index_sesi_pb`='{$index_sesi_pb}'
SQL;

//                $allSQL= '<UPD SUMBER>';
//                $allSQL.= $sqlUpdObjSrcVal;
//                echo $allSQL;exit();
                $DBCmd->executeCustomQueryCommand($sqlUpdObjSrcVal);
                
                if ($DBCmd->getExecutionStatus() === true) {
//                if (true) {
                    /* TODO: Kemaskini destinasi */

                    $ObjDesNewVal = $ObjDesOriginalVal + $nilai_terimaan_objek_destinasi;

$sqlUpdObjDesVal = <<<SQL
UPDATE
  `tbl_prestasi_belanja`
SET
  `peruntukan_dipinda` = '$ObjDesNewVal',
  `edit_by` = 'SISTEM - TP',
  `last_edit_datetime` = NOW()
WHERE `kod_objek` = '$kod_objek_destinasi'
  AND `id_aktiviti` = '$id_aktivity'
  AND `index_sesi_pb`='{$index_sesi_pb}'
SQL;

//                    $allSQL= '<UPD DES>';
//                    $allSQL.= $sqlUpdObjDesVal;
//                    echo $allSQL;exit();
                    $DBCmd->executeCustomQueryCommand($sqlUpdObjDesVal);

                    if ($DBCmd->getExecutionStatus() === true) {
//                    if (true) {    
                    /*TODO: Update back permohonan status*/
                        
$sqlUpdPermohonanStatus = <<<SQL
UPDATE
  `tbl_permohonan`
SET
  `tarikh_peraku_kedua` = NOW(),
  `id_pegawai_peraku_kedua` = '{$_SESSION["icno"]}',
  `catatan_peraku2` = '{$catatan_peraku2}',
  `status_permohonan` = 6
WHERE `index_permohonan` = '$index_permohonan';
SQL;
                        
//                        $allSQL= '<UPD STATUS>';
//                        $allSQL.= $sqlUpdPermohonanStatus;
//                        echo $allSQL;exit();
                        $DBCmd->executeCustomQueryCommand($sqlUpdPermohonanStatus);
                        
                        if ($DBCmd->getExecutionStatus() === true) {                            
                            
                        }else{
                            $errorMsg = 'Kemaskini Status Permohonan gagal!';
                            exit();
                        }
                    }else{
                        $errorMsg = 'Kemaskini Nilai Objek (Destinasi) gagal!';
                        exit();
                    }
                } else {
                    $errorMsg = 'Kemaskini Nilai Objek (Sumber) gagal!';
                    exit();
                }
            }
            //Only commit if all previous trans successfull
            if ($DBCmd->commitTransaction() === true) {
                $obj = new MagicObject();
                $obj->operationMessage = 'Berjaya Diperakukan xxxx';
                $DBCmd->disableTransaction();
                echo $obj->getJsonString();
                exit();
            } else {
                $DBCmd->disableTransaction();
                $errorMsg = 'Transaksi gagal disimpan!';
            }
        }       
    }
    echo $errorMsg;
//    header("{$_SERVER['SERVER_PROTOCOL']} 500 $errorMsg");
//    exit;
}    