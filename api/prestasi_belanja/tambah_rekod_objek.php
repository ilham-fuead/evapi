<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,Authorization');

include_once '../../vendor/autoload.php';
include_once '../login/EVSessionHandler.php';
include_once '../config/db_connection.php';
include_once '../config/KodTransaksi.php';

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    echo 'preflight';exit;
}

$DBQueryObj = new DBQuery($host, $username, $password, $database_name);

/*TODO: Initializing session*/
$headers = apache_request_headers();

if($headers){
    $session_id= mysqli_real_escape_string($DBQueryObj->getLink(), $headers['Authorization']);
    
    $sessionHandler=new EVSessionHandler($DBQueryObj);
    session_set_save_handler($sessionHandler, true);
    session_id($session_id);
    session_start();
    
    if(!isset($_SESSION['icno'])){
        header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
        echo 'Sesi tidak sah!';
        exit();
    }else{
        if(!in_array($_SESSION['roleID'], [2,3,4,5,11])){
            header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
            echo 'Peranan tidak sah';
            exit();
        }
    }
}else{
    header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
    echo 'Sesi tidak sah!';
    exit();
}

$MagicInputObj = new MagicInput();
$MagicInputObj->copy_RAW_JSON_properties();
$DBQueryObj = new DBQuery($host, $username, $password, $database_name);

$kod_objek= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->kod_objek);
$id_aktiviti= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->kod_aktiviti);
$id_program= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->kod_program);
$peruntukan_asal= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->asal);
$peruntukan_dipinda= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->dipinda);
$perbelanjaan_sebenar= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->sebenar);
$perbelanjaan_tanggungan= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->tanggungan);
$kategori_kod_objek= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->kategori_kod_objek);
$add_by=$_SESSION['icno'];

/*TODO:Get PB current ID */
$index_sesi_pb='';

$sqlCurrentPB=<<<SQL
SELECT
  `index_sesi_pb`  
FROM
  `tbl_tetapan_pb`
WHERE status_sesi=1
SQL;

$DBQueryObj->setSQL_Statement($sqlCurrentPB);

$DBQueryObj->runSQL_Query();

if($DBQueryObj->isHavingRecordRow()){
    while($row=$DBQueryObj->fetchRow()){
        /* Manipulating array $row here */
        $index_sesi_pb=$row['index_sesi_pb'];
    }
}else{
    header("{$_SERVER['SERVER_PROTOCOL']} 503 Locked");
    echo 'Tiada Prestasi Belanja yang aktif!';
    exit();
}

$sqlQuery = <<<SQL
SELECT
  `index_PB`,
  `id_program`,
  `nama_program`,
  `id_aktiviti`,
  `nama_aktiviti`,
  `kod_objek`,
  `perihal_objek`,
  `peruntukan_asal`,
  `peruntukan_dipinda`,
  `perbelanjaan_sebenar`,
  `perbelanjaan_tanggungan`,
  `kategori_kod_objek`
FROM
  `tbl_prestasi_belanja`
WHERE `kod_objek`='{$kod_objek}'
AND `id_aktiviti` ='{$id_aktiviti}'
AND `index_sesi_pb`='{$index_sesi_pb}'
SQL;

$DBQueryObj->setSQL_Statement($sqlQuery);
$DBQueryObj->runSQL_Query();

if($DBQueryObj->isHavingRecordRow()){
    $err='HTTP/1.1 406 Rekod Prestasi Belanja telah wujud! ' . $id_aktiviti;
    header($err);    
    exit();
}

$sql = <<<SQL
INSERT INTO `tbl_prestasi_belanja` (
    `id_program`,
    `id_aktiviti`,
    `kod_objek`,
    `peruntukan_asal`,
    `peruntukan_dipinda`,
    `perbelanjaan_sebenar`,
    `perbelanjaan_tanggungan`,
    `kategori_kod_objek`,
    `add_by`,
    `add_datetime`,
    `index_sesi_pb`
  )
  VALUES
    (
      '$id_program',
      '$id_aktiviti',
      '$kod_objek',
      '$peruntukan_asal',
      '$peruntukan_dipinda',
      '$perbelanjaan_sebenar',
      '$perbelanjaan_tanggungan',
      '$kategori_kod_objek',
      '$add_by',
      NOW(),
      '$index_sesi_pb'
    );
SQL;

$DBCmd = new DBCommand($DBQueryObj);
$DBCmd->executeCustomQueryCommand($sql);

/* Check if command is successfull */
if ($DBCmd->getExecutionStatus() === true) {    
    
    /* Retrieving affected row count on update */
    $rowCount = $DBCmd->getAffectedRowCount();
    //echo "Successfully! $rowCount records updated!";
    
    if($rowCount!=-1){        
        echo "Successfully updated!";
    }else{
        echo "Operation failed!";
    }
    
} else {
    $error_no = $DBCmd->getErrno();
    $error_message = $DBCmd->getError();
    echo "$error_no: $error_message";
}
