<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,Authorization');

include_once '../../vendor/autoload.php';
include_once '../config/db_connection.php';

$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

$GET_Data=new MagicInput();
$GET_Data->copy_GET_properties();

$condition='';

/**TODO: Filter **/
if(!is_null($GET_Data->id_aktiviti)  && $GET_Data->id_aktiviti!==''){
    $id_aktiviti= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->id_aktiviti);
    $condition.="WHERE id_aktiviti = '$id_aktiviti'";    
}

if(!is_null($GET_Data->kategori_kod_objek) && $GET_Data->kategori_kod_objek!==''){
    $kategori_kod_objek= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->kategori_kod_objek);
    
    if($condition!=''){
        $condition.=" AND kategori_kod_objek = '$kategori_kod_objek'";
    }else{
        $condition.="WHERE kategori_kod_objek = '$kategori_kod_objek'";
    }    
}

/*TODO:Get PB current active ID */
$index_sesi_pb='';

$sqlCurrentPB=<<<SQL
SELECT
  `index_sesi_pb`  
FROM
  `tbl_tetapan_pb`
WHERE status_sesi=1
SQL;

$DBQueryObj->setSQL_Statement($sqlCurrentPB);

$DBQueryObj->runSQL_Query();

if($DBQueryObj->isHavingRecordRow()){
    while($row=$DBQueryObj->fetchRow()){
        /* Manipulating array $row here */
        $index_sesi_pb=$row['index_sesi_pb'];
    }
}else{
    header("{$_SERVER['SERVER_PROTOCOL']} 503 Locked");
    echo 'Tiada Prestasi Belanja yang aktif!';
    exit();
}

/*TODO:Include current PB id in query */
if ($condition != '') {
    $condition .= " AND p.index_sesi_pb = '$index_sesi_pb'";
} else {
    $condition .= "WHERE p.index_sesi_pb = '$index_sesi_pb'";
}

$sql=<<<SQL
SELECT
  `index_PB`,
  `id_program`,
  `nama_program`,
  `id_aktiviti`,
  `nama_aktiviti`,
  `kod_objek`,
  (SELECT `perihal_objek` FROM `EVirement`.`tbl_objek_ref` WHERE `kod_objek`=p.kod_objek) AS `perihal_objek`,
  `peruntukan_asal`,
  `peruntukan_dipinda`,
  `perbelanjaan_sebenar`,
  `perbelanjaan_tanggungan`,
  `kategori_kod_objek`,
  (peruntukan_dipinda-perbelanjaan_sebenar-perbelanjaan_tanggungan) AS baki,
  ((perbelanjaan_sebenar/peruntukan_dipinda)*100) AS peratus_belanja_tanpa_tanggungan,
  (((perbelanjaan_sebenar+perbelanjaan_tanggungan)/peruntukan_dipinda)*100) AS peratus_belanja_dengan_tanggungan
FROM
  `tbl_prestasi_belanja` p
$condition ORDER BY kod_objek ASC
SQL;

$DBQueryObj->setSQL_Statement($sql);

$DBQueryObj->runSQL_Query();

if($DBQueryObj->isHavingRecordRow()){
    echo $DBQueryObj->getRowsInJSON();
}else{
    echo '[]';
}
