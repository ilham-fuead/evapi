<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,Authorization');

/*TODO: (1) include all security headers above*/
include_once '../../vendor/autoload.php';

/*TODO: (2) Include EV Session Container Class*/
include_once '../login/EVSessionHandler.php';

include_once '../config/db_connection.php';

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    echo 'preflight';exit;
}

$DBQueryObj = new DBQuery($host, $username, $password, $database_name);

/*TODO: (3) Read authentication token from front-end request*/
$headers = apache_request_headers();

/*TODO: (4) If app server capture any request header, proceed with authentication*/
if($headers){
    /*TODO: (5) Read header authorization from api request and set as session id*/
    $session_id= mysqli_real_escape_string($DBQueryObj->getLink(), $headers['Authorization']);
    
    $sessionHandler=new EVSessionHandler($DBQueryObj);
    session_set_save_handler($sessionHandler, true);
    session_id($session_id);
    session_start();
    
    if(!isset($_SESSION['icno'])){
        /*TODO: (6) Authentication failed, user is not in session*/
        header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
        echo 'Sesi tidak sah!';
        exit();
    }else{
        if(!in_array($_SESSION['roleID'], [2,3,4,5,11])){
            /*TODO: (6) Authorization failed, user is in session but lack of required access role*/
            header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
            echo 'Peranan tidak sah';
            exit();
        }
    }
}else{
    /*TODO: No header sent by requester or app server failed reading request header*/
    header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
    echo 'Sesi tidak sah!';
    exit();
}

/*TODO: (7) Authentication & Authorization is successfull, proceed with api logic*/

$MagicInputObj = new MagicInput();
$MagicInputObj->copy_RAW_JSON_properties();

$itemPermohonanOBJ = $MagicInputObj->getJsonString();

$itemPermohonanOBJ = json_decode($itemPermohonanOBJ, true);

// $kategori_simpanan_data=2;

foreach ($itemPermohonanOBJ as $value)
{
	$kategori_simpanan_data=2;

	$index_item= mysqli_real_escape_string($DBQueryObj->getLink(), $value['index_item']);

$sql = <<<SQL
UPDATE 
`tbl_item_permohonan` 
SET
  `kategori_simpanan_data` = $kategori_simpanan_data
WHERE `index_item` = '{$index_item}'
SQL;

//echo $sql;exit;

$DBCmd = new DBCommand($DBQueryObj);
$DBCmd->executeCustomQueryCommand($sql);

}

foreach ($itemPermohonanOBJ as $value)
{
	$kategori_simpanan_data=1;

	$kod_objek_sumber= mysqli_real_escape_string($DBQueryObj->getLink(), $value['kod_objek_sumber']);
	$kod_objek_destinasi= mysqli_real_escape_string($DBQueryObj->getLink(), $value['kod_objek_destinasi']);
	$nilai_baki_asal_objek_sumber= mysqli_real_escape_string($DBQueryObj->getLink(), $value['nilai_baki_asal_objek_sumber']);
	$nilai_baki_asal_objek_destinasi= mysqli_real_escape_string($DBQueryObj->getLink(), $value['nilai_baki_asal_objek_destinasi']);
	$nilai_terimaan_objek_destinasi= mysqli_real_escape_string($DBQueryObj->getLink(), $value['nilai_terimaan_objek_destinasi']);
	$nilai_pindahan_objek_sumber= mysqli_real_escape_string($DBQueryObj->getLink(), $value['nilai_pindahan_objek_sumber']);
	$index_permohonan= mysqli_real_escape_string($DBQueryObj->getLink(), $value['index_permohonan']);



$sql = <<<SQL
INSERT INTO `tbl_item_permohonan` (
  `index_permohonan`,
  `kod_objek_sumber`,
  `nilai_baki_asal_objek_sumber`,
  `nilai_pindahan_objek_sumber`,
  `kod_objek_destinasi`,
  `nilai_baki_asal_objek_destinasi`,
  `nilai_terimaan_objek_destinasi`,
  `kategori_simpanan_data`
) 
VALUES
  (
  	'{$index_permohonan}',
    '{$kod_objek_sumber}',
    '{$nilai_baki_asal_objek_sumber}',
    '{$nilai_pindahan_objek_sumber}',
    '{$kod_objek_destinasi}',
    '{$nilai_baki_asal_objek_destinasi}',
    '{$nilai_terimaan_objek_destinasi}',
    '{$kategori_simpanan_data}'
  ) ;
SQL;

$DBCmd = new DBCommand($DBQueryObj);
$DBCmd->executeCustomQueryCommand($sql);

}

/* Check if command is successfull */
if ($DBCmd->getExecutionStatus() === true) {
    /* Retrieving affected row count on update */
    $rowCount = $DBCmd->getAffectedRowCount();

    //echo "Successfully! $rowCount records updated!";
    $obj = new MagicObject();
    $obj->status = $DBCmd->getAffectedRowCount();
    $obj->errorMessage = 'Berjaya';
    echo $obj->getJsonString();
} else {
    $error_no = $DBCmd->getErrno();
    $error_message = $DBCmd->getError();
    echo "$error_no: $error_message";
}